from pwn import *
import pyshark

cap = pyshark.FileCapture('public/ananas.pcapng', display_filter='tcp.port == 18812 && tcp.payload')

streams = {}

def i32(n):
	n = n % 0x100000000
	return n

def display(v):
	seed = u32(v[0:4])
	data = v[4:].ljust(160*90, b"\xff")

	print(len(data))

	def rand():
		nonlocal seed
		seed = i32(seed * 48192 + 4717718)
		seed ^= seed >> 7
		seed ^= i32(seed << 17)
		seed ^= i32(seed*77)
		seed = i32(seed)

		return seed // 1234 % 0x10000 

	test = [i for i in range(160*90)]
	for i in range(160*90-1, 0, -1):
		j = rand() % i
		test[i], test[j] = test[j], test[i]
	unsc = [None] * (160*90)
	for i in range(160*90):
		unsc[test[i]] = data[i]

	R = ""
	for r in range(90):
		for c in range(160):
			R += [" ","_","▁","▂","▃","▄","▅","▆","▇","█"][unsc[r*160+c]//26]*2
		R += "\n"
	print(R)

for pkt in cap:
	if 'tcp' not in pkt or not hasattr(pkt.tcp, "payload"): continue
	#print(pkt.number, "/", N)
	s = int(pkt.tcp.stream)
	print(s)
	if s not in streams: streams[s] = b''
	#print(pkt.tcp.payload)
	streams[s] += bytes.fromhex(pkt.tcp.payload.replace(":",""))

	if len(streams[s]) == 160*90 + 5:
		display(streams[s])
