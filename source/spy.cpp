#include <windows.h>
#include "vilibs/videoInput/videoInput.h"
#include <stdlib.h>

namespace mr {
	unsigned int seed;

	unsigned short rand() {
		seed = seed * 48192 + 4717718;
		seed ^= seed >> 7;
		seed ^= seed << 17;
		seed ^= seed * 77;

		return seed / 1234 % 0x10000;
	}
}

//134.209.225.118 0x76e1d186
const struct sockaddr_in server = {
	.sin_family = AF_INET,
	.sin_port = htons(18812),
	.sin_addr = {.S_un = {.S_addr = 0x4b435546}}/*/0x76e1d186}}*/
};

void shuf(char *data, const int size) {
    for(int i=size-1; i>0; i--) {
        int j = mr::rand() % i;
        int t = data[i];
        data[i] = data[j];
        data[j] = t;
    }
}

void connect_send(char *data, const int size) {
	SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(connect(s, ((sockaddr *)&server), sizeof(server))) exit(0);

	//printf("Connected!\n");

	unsigned int seed;
	if(!recv(s, (char*)&seed, 4, 0)) exit(0);

	//printf("Received seed %d\n", seed);
	mr::seed = seed;
    for(int i=size-1; i>0; i--) {
        int j = mr::rand() % i;
        int t = data[i];
        data[i] = data[j];
        data[j] = t;
    }
	int total_sent = 0, num_sent;
	while(total_sent < size && (num_sent = send(s, data+total_sent, size-total_sent, 0)) != SOCKET_ERROR) {
        total_sent += num_sent;
        //printf("Sent %d (+%d)!\n", total_sent, num_sent);

	}
	if(num_sent == SOCKET_ERROR) {
        exit(0);
	}
	char ack;
	recv(s, &ack, 1, 0);
    Sleep(10);  // Wait for send
	closesocket(s);
}


const int w = 160, h = 90;
char result[w*h];
WSADATA wsaData;
unsigned char buf[w*h*3];
int main() {
    WSAStartup(MAKEWORD(2,2), &wsaData);

	std::vector <std::string> devices = videoInput::getDeviceList();
	if(devices.size() == 0) {
        connect_send("NODEV", 5);
        return 0;
	}

	videoInput VI;
	VI.setupDevice(0, w, h);

	for(int i=0; i<10; i++) {
        while(!VI.isFrameNew(0)) Sleep(10);
        VI.getPixels(0, buf);
    }

	while(1) {
        Sleep(350);
        while(!VI.isFrameNew(0)) Sleep(10);
        VI.getPixels(0, buf, true, true);

        for(int i=0; i<h; i++)
        for(int j=0; j<w; j++) {
               result[i*w+j] = (
                        (int)buf[(i*w+j)*3+0] +
                        (int)buf[(i*w+j)*3+1] +
                        (int)buf[(i*w+j)*3+2]
                    )/3;
        }
        connect_send(result, w*h);
	}

	return 0;
}
